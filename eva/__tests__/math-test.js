const assert = require('assert');
const {test} = require('./test-util');

module.exports = eva => {
	test(eva, `(+ 1 5)`, 6);
	test(eva, `(+ (+ 3 2) 5)`, 10);
	test(eva, `(+ (* 3 2) 5)`, 11);
};
